val secrets = getSecrets()

tasks.register("duckdns") {
    doLast {
        val token = secrets.duckdnsToken
        val container = """[Container]
Image=docker.io/curlimages/curl:latest
Exec=curl --verbose --get https://www.duckdns.org/update --data-urlencode domains=samske --data-urlencode token=${token} --fail-with-body
AutoUpdate=registry

[Service]
Type=oneshot
"""
        val timer = """[Timer]
# Every 5 minutes
OnCalendar=*-*-* *:00/5:00

[Install]
WantedBy=timers.target
"""
        val containerPath = containersDir.resolve("duckdns.container")
        fileExists(containerPath, posixFilePermissions("rw-r-----"))
        val containerChange = fileContent(containerPath, container)
        val timerChange = fileContent(systemdDir.resolve("duckdns.timer"), timer)
        if (containerChange || timerChange) {
            exec {
                commandLine("systemctl", "daemon-reload")
            }
        }
        if (timerChange) {
            exec {
                commandLine("systemctl", "enable", "--now", "duckdns.timer")
            }
        }
    }
}

tasks.register("wg-server") {
    doLast {
        val peers = sequenceOf(
            "t7K0C9dPL56wMSGcb7o52ym9YFTa5yml1FNinxWXHRU=" to "10.33.0.2/32",  // ares
            "ol7iBqc1p3Jexc5KSmlP5IGspt8tdOAxyM5zflrHBAU=" to "10.33.0.3/32",  // fp3
            "s6Di4qejxbE1lxhE0ToWETPJoCQqA/xjLzOkriag+Ao=" to "10.33.0.4/32",  // lyotard
        )
        val config = """[Interface]
ListenPort = 51820
Address = 10.33.0.1/24
PrivateKey = ${secrets.kubeAgeSecretKey}
PostUp = iptables --table nat --append POSTROUTING --source 10.33.0.0/24 --out-interface eth0 --jump MASQUERADE
PostDown = iptables --table nat --delete POSTROUTING --source 10.33.0.0/24 --out-interface eth0 --jump MASQUERADE
""" + peers.map{"""[Peer]
PublicKey = ${it.first}
AllowedIPs = ${it.second}
"""}.joinToString("\n", "\n")
        val container = """[Install]
WantedBy=default.target

[Service]
Restart=always

[Container]
AddCapability=NET_ADMIN NET_RAW
PublishPort=51820:51820/udp
Sysctl=net.ipv4.conf.all.forwarding=1
Volume=./wireguard/wg0.conf:/etc/wireguard/wg0.conf
Image=docker.io/procustodibus/wireguard:v1
AutoUpdate=registry
"""
        val configPath = containersDir.resolve("wireguard/wg0.conf")
        fileExists(configPath, posixFilePermissions("rw-r-----"))
        val configChange = fileContent(configPath, config)
        val containerChange = fileContent(containersDir.resolve("wireguard.container"), container)
        if (!configChange && !containerChange) return@doLast
        exec {
            commandLine("systemctl", "daemon-reload")
        }
        exec {
            commandLine("systemctl", "restart", "wireguard.service")
        }
    }
}

infraTask("remoting", "duckdns", "wg-server")
