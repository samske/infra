val secrets = getSecrets()

fun connection(
    name: String,
    publicKey: String,
    endpoint: String,
    allowedIps: String,
    addresses: Map<String, String>,
    dns: String,
    dnsSearch: String,
) = run {
    tasks.register(name) {
        doLast {
            val hostname = java.net.InetAddress.getLocalHost().getHostName()
            val uuid = java.util.UUID.nameUUIDFromBytes("${name}@${hostname}".toByteArray())
            val privateKey = secrets.wgPrivateKeys[hostname] ?:
                throw IllegalArgumentException("no wg private key for ${hostname}: podman run --rm --entrypoint wg lscr.io/linuxserver/wireguard genkey")
            val address = addresses[hostname] ?:
                throw IllegalArgumentException("no wg address for ${hostname}")
            val connection = """[connection]
id=${name}
uuid=${uuid}
type=wireguard
autoconnect=false
interface-name=${name}
permissions=user:${userName}:;

[wireguard]
private-key=${privateKey}

[wireguard-peer.${publicKey}]
endpoint=${endpoint}
allowed-ips=${allowedIps}

[ipv4]
address1=${address}
dns=${dns}
dns-search=${dnsSearch}
method=manual
never-default=true

[ipv6]
addr-gen-mode=stable-privacy
method=disabled

[proxy]
"""
            val connectionPath = etcDir.resolve("NetworkManager/system-connections/${name}.nmconnection")
            fileExists(connectionPath, posixFilePermissions("rw-------"))
            val connectionChange = fileContent(connectionPath, connection)
            if (!connectionChange) return@doLast
            exec {
                commandLine("nmcli", "connection", "load", connectionPath)
            }
        }
    }
}

connection(
    "wgsamske",
    "N/Jl27TerhBpFzzibqCArrCM5170/xY1ZeqBhB1URy8=",
    "samske.duckdns.org:51820",
    "10.96.0.0/16;",
    mapOf(
        "ares" to "10.33.0.2/32",
        "fp3" to "10.33.0.3/32",
        "lyotard" to "10.33.0.4/32",
        "zeus" to "10.33.0.5/32",
    ),
    "10.96.0.10;",
    "~cluster.local;",
)

connection(
    "wgptl",
    "McvligMmAg5mQ/PpEsB6mLWgqPV8k1cr1T7Jt1KH3VI=",
    "posttenebraslab.ch:51820",
    "10.42.0.0/16;",
    mapOf(
        "ares" to "10.10.0.2/32",
        "fp3" to "10.10.0.5/32",
        "lyotard" to "10.10.0.6/32",
        "zeus" to "10.10.0.13/32",
    ),
    "10.42.0.1;",
    "~lan.posttenebraslab.ch;",
)

infraTask("wireguard", "wgsamske", "wgptl")
