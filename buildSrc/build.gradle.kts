repositories {
    mavenCentral()
}

plugins {
    `kotlin-dsl`
}

dependencies {
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.16.+")
    implementation("com.fasterxml.jackson.dataformat:jackson-dataformat-yaml:2.16.+")
}
