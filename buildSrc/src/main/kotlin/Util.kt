import java.nio.file.Files
import java.nio.file.NoSuchFileException
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.StandardOpenOption
import java.nio.file.attribute.FileAttribute
import java.nio.file.attribute.FileOwnerAttributeView
import java.nio.file.attribute.PosixFilePermissions
import com.fasterxml.jackson.module.kotlin.registerKotlinModule

val userName = "marv"
val userId = 1000
var rootDir = Paths.get("/")
val homeDir = rootDir.resolve("var/home")
val userHomeDir = homeDir.resolve(userName)
val runtimeDir = rootDir.resolve("run")
val userRuntimeDir = runtimeDir.resolve("user").resolve("${userId}")
val etcDir = rootDir.resolve("etc")
val containersDir = etcDir.resolve("containers").resolve("systemd")
val systemdDir = etcDir.resolve("systemd").resolve("system")

fun existingAttrs(path: Path, vararg attrs: FileAttribute<out Any>) = run {
    existingAttrs(path, attrs.asSequence())
}
fun existingAttrs(path: Path, attrs: Sequence<FileAttribute<out Any>>) = run {
    val attrsChange = attrs.map{
        val attribute = it.name()
        val split = attribute.split(':', limit=2)
        if (split.size > 1) {
            val (view, name) = split
            Triple(view, name, it)
        } else {
            Triple("basic", attribute, it)
        }
    }.groupBy({it.first},{it.second to it.third}).mapValues{(view, attrsTarget)->
        val attrsActual = try { Files.readAttributes(path, attrsTarget.joinToString(",", "${view}:"){it.first}) } catch (e: NoSuchFileException) { emptyMap() }
        attrsTarget.filter{attrsActual[it.first] != it.second.value()}
    }.values.flatten().map{it.second}

    if (attrsChange.isEmpty()) {
        return@run false
    }

    attrsChange.forEach {
        println("setAttribute ${it.name()}, ${it.value()}")
        Files.setAttribute(path, it.name(), it.value())
    }
    true
}

fun posixFilePermissions(perms: String) = PosixFilePermissions.asFileAttribute(PosixFilePermissions.fromString(perms))

fun copyOwnerGroup(path: Path, from: Path) = run {
    val ownerGroup = Files.readAttributes(from, "posix:owner,group").asSequence().map{
        object: FileAttribute<Any> {
            override fun name() = "posix:${it.key}"
            override fun value() = it.value
        }
    }
    existingAttrs(path, ownerGroup)
}

fun dirExists(path: Path, vararg attrs: FileAttribute<out Any>): Boolean = run {
        if (Files.exists(path)) {
        return@run existingAttrs(path, *attrs)
    }

    val parent = path.parent
    dirExists(parent)

    println("createDirectory ${path}")
    Files.createDirectory(path, *attrs)
    copyOwnerGroup(path, parent)
    true
}

fun fileExists(path: Path, vararg attrs: FileAttribute<out Any>): Boolean = run {
    if (Files.exists(path)) {
        return@run existingAttrs(path, *attrs)
    }

    val parent = path.parent
    dirExists(parent)

    println("createFile ${path}")
    Files.createFile(path, *attrs)
    copyOwnerGroup(path, parent)
    true
}

fun fileLine(path: Path, line: String) = run {
    if (Files.exists(path)) {
        if (Files.lines(path).anyMatch{it == line}) return@run false
    }

    fileExists(path)
    println("writeString ${path}")
    Files.writeString(path, "${line}\n", StandardOpenOption.APPEND)
    true
}

fun fileContent(path: Path, content: String) = run {
    if (Files.exists(path)) {
        if (Files.readString(path) contentEquals content) return@run false
    }

    fileExists(path)
    println("writeString ${path}")
    Files.writeString(path, content)
    true
}

fun org.gradle.api.Project.infraTask(name: String, vararg deps: String) = run {
    tasks.register(name) {
        group = "infra"
        description = deps.joinToString(", ")
        dependsOn(*deps)
    }
}

@com.fasterxml.jackson.databind.annotation.JsonNaming(com.fasterxml.jackson.databind.PropertyNamingStrategies.SnakeCaseStrategy::class)
data class Secrets(
    val kubeAgeSecretKey: String,
    val duckdnsToken: String,
    val wgPrivateKeys: Map<String, String>,
)

fun org.gradle.api.Project.getSecrets() = run {
    val keys = layout.projectDirectory.file("keys.txt")
    val secrets = layout.projectDirectory.file("secrets.yaml")

    val output = java.io.ByteArrayOutputStream()
    exec {
        commandLine("podman", "run", "--rm", "--privileged", "--volume", "${keys}:${keys}", "--volume", "${secrets}:${secrets}", "--env", "SOPS_AGE_KEY_FILE=${keys}", "quay.io/getsops/sops:v3.8.1", "--decrypt", "${secrets}")
        standardOutput = output
    }
    val input = output.toByteArray().inputStream()

    val mapper = com.fasterxml.jackson.databind.ObjectMapper(com.fasterxml.jackson.dataformat.yaml.YAMLFactory()).registerKotlinModule()
    mapper.readValue(input, Secrets::class.java)
}

val ignoreOutput = object: java.io.OutputStream() {
    override fun write(b: Int) = run {}
    override fun write(b: ByteArray) = run {}
    override fun write(b: ByteArray, off: Int, len: Int) = run {}
}
