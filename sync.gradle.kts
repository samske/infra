tasks.register("linger") {
    doLast {
        val output = java.io.ByteArrayOutputStream()
        exec {
            commandLine("loginctl", "show-user", userName)
            standardOutput = output
        }

        val properties = java.util.Properties()
        properties.load(output.toByteArray().inputStream())
        if (properties["Linger"] == "yes") return@doLast

        exec {
            commandLine("loginctl", "enable-linger", userName)
        }
    }
}

tasks.register("syncthing") {
    dependsOn("linger")
    doLast {
        dirExists(userHomeDir.resolve(".config/syncthing"))
        dirExists(userHomeDir.resolve("@"))
        val containerChange = fileContent(userHomeDir.resolve(".config/containers/systemd/syncthing.container"), """[Install]
    WantedBy=default.target

    [Unit]
    After=network.target

    [Service]
    # https://github.com/systemd/systemd/issues/3312#issuecomment-2185399471
    ExecStartPre=sh -c 'until systemctl is-active network-online.target; do sleep 1; done'

    [Container]
    Image=docker.io/syncthing/syncthing:1
    AutoUpdate=registry
    PublishPort=8384:8384
    PublishPort=21027:21027/udp
    PublishPort=22000:22000/udp
    UserNS=keep-id:uid=1000,gid=1000
    SecurityLabelDisable=true
    Volume=%h/.config/syncthing:/var/syncthing/config
    Volume=%h/@:/var/syncthing/@
    """)
        if (containerChange) {
            println("systemctl restart syncthing")
            exec {
                commandLine("sudo", "--user", userName, "XDG_RUNTIME_DIR=${userRuntimeDir}", "systemctl", "--user", "daemon-reload")
            }
            exec {
                commandLine("sudo", "--user", userName, "XDG_RUNTIME_DIR=${userRuntimeDir}", "systemctl", "--user", "restart", "syncthing.service")
            }
        }
    }
}

infraTask("sync", "linger", "syncthing")
