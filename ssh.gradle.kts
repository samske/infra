import java.nio.file.Files
import java.nio.file.Paths

tasks.register("sshd") {
    doLast {
        exec {
            commandLine("systemctl", "enable", "--now", "sshd.service")
        }
    }
}

val ssh = userHomeDir.resolve(".ssh")
val pub = ssh.resolve("id_ed25519.pub")

tasks.register("ssh-keygen") {
    doLast {
        val id = ssh.resolve("id_ed25519")
        if (Files.exists(id) && Files.exists(pub)) return@doLast
        exec {
            commandLine("ssh-keygen", "-t", "ed25519", "-f", id)
        }
    }
}

tasks.register("known_hosts") {
    dependsOn("sshd")
    doLast {
        val hostname = java.net.InetAddress.getLocalHost().getHostName()
        val localhost = Files.readString(etcDir.resolve("ssh/ssh_host_ed25519_key.pub")).trim()
        val line = "${hostname} ${localhost}"
        fileLine(ssh.resolve("known_hosts"), line)
        fileLine(layout.projectDirectory.file("known_hosts").asFile.toPath(), line)
    }
}

tasks.register("authorized_keys") {
    dependsOn("ssh-keygen")
    doLast {
        val line = Files.readString(pub).trim()
        val source = layout.projectDirectory.file("authorized_keys").asFile.toPath()
        val target = ssh.resolve("authorized_keys")
        fileLine(source, line)
        if (Files.exists(target) && Files.mismatch(source, target) == -1L) return@doLast
        Files.copy(source, target, java.nio.file.StandardCopyOption.REPLACE_EXISTING)
    }
}

tasks.register("llmnr") {
    doLast {
        val llmnrConf = """[Resolve]
LLMNR=yes
"""
        if (!fileContent(etcDir.resolve("systemd/resolved.conf.d/llmnr.conf"), llmnrConf)) {
            return@doLast
        }
        exec {
            commandLine("systemctl", "restart", "systemd-resolved.service")
        }
    }
}

infraTask("ssh", "ssh-keygen", "authorized_keys", "known_hosts", "sshd", "llmnr")
